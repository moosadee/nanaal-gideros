NG_STATE_MANAGER = Core.class()

function NG_STATE_MANAGER:init() end

function NG_STATE_MANAGER:SETUP()
	NG_DEBUG:LOG( "NG_STATE_MANAGER:SETUP", "info", "Initialize state manager" )
	NG_STATE_MANAGER.STATES = {}
	NG_STATE_MANAGER.CURRENT_STATE = nil
end

function NG_STATE_MANAGER:ADD_STATE( stateKey, stateObject )
	NG_DEBUG:LOG( "NG_STATE_MANAGER:ADD_STATE", "info", "Added state \"" .. stateKey .. "\"" )
	NG_STATE_MANAGER.STATES[ stateKey ] = stateObject
end

function NG_STATE_MANAGER:GOTO_STATE( stateKey )
	NG_DEBUG:LOG( "NG_STATE_MANAGER:GOTO_STATE", "info", "Go to state \"" .. stateKey .. "\"" )
	
	-- Check if this state exists
	if ( NG_STATE_MANAGER.STATES[ stateKey ] == nil ) then
		NG_DEBUG:LOG( "NG_STATE_MANAGER:GOTO_STATE", "error", "State \"" .. stateKey .. "\" does not exist" )
		return
	end
	
	if ( NG_STATE_MANAGER.CURRENT_STATE ~= nil ) then
		NG_DEBUG:LOG( "NG_STATE_MANAGER:GOTO_STATE", "info", "Clean up old state and free any registered assets" )
		-- Destroy old state
		NG_STATE_MANAGER.CURRENT_STATE:Stop()
		NG_ASSET_MANAGER:CLEAR_TEXTURES()
		NG_ASSET_MANAGER:CLEAR_FONTS()
	end
	
	-- Load in new state, Setup / Run
	NG_STATE_MANAGER.CURRENT_STATE = NG_STATE_MANAGER.STATES[ stateKey ]
	NG_STATE_MANAGER.CURRENT_STATE:Start()
end

function NG_STATE_MANAGER:GET_CURRENT_STATE()
    return NG_STATE_MANAGER.CURRENT_STATE
end
