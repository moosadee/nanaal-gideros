NG_LANGUAGE_MANAGER = Core.class()

function NG_LANGUAGE_MANAGER:init()
	NG_LANGUAGE_MANAGER.LINES = {}
end

function NG_LANGUAGE_MANAGER:LOAD_LANGUAGE_FILE( options )
	NG_DEBUG:LOG( "NG_LANGUAGE_MANAGER:LOAD_LANGUAGE_FILE", "clutter", 
		"Add language \"" .. options.key .. "\" at path \"" .. options.path .. "\"" )
	-- key, path
	NG_LANGUAGE_MANAGER:_READ_FILE( options )
end

function NG_LANGUAGE_MANAGER:_READ_FILE( options )
	local giderosPath = "|R|" .. options.path

	NG_LANGUAGE_MANAGER.LINES[ options.key ] = {}
		
	for line in io.lines( giderosPath ) do
		local columns = ParseCSVLine( line, "," )
		NG_LANGUAGE_MANAGER.LINES[ options.key ][ columns[1] ] = columns[2]
	end
end

function NG_TEXT( key, text )
	if ( NG_LANGUAGE_MANAGER.LINES[ key ] == nil ) then
		NG_DEBUG:LOG( "NG_TEXT", "clutter", 
			"Language key \"" .. key .. "\" does not exist" )
		return nil
	
	elseif ( NG_LANGUAGE_MANAGER.LINES[ key ][ text ] == nil ) then
		NG_DEBUG:LOG( "NG_TEXT", "clutter", 
			"Text key \"" .. text .. "\" does not exist" )
		return nil
	
	end
	
	return NG_LANGUAGE_MANAGER.LINES[ key ][ text ]
end

-- http://lua-users.org/wiki/LuaCsv
function ParseCSVLine (line,sep) 
	local res = {}
	local pos = 1
	sep = sep or ','
	while true do 
		local c = string.sub(line,pos,pos)
		if (c == "") then break end
		if (c == '"') then
			-- quoted value (ignore separator within)
			local txt = ""
			repeat
				local startp,endp = string.find(line,'^%b""',pos)
				txt = txt..string.sub(line,startp+1,endp-1)
				pos = endp + 1
				c = string.sub(line,pos,pos) 
				if (c == '"') then txt = txt..'"' end 
				-- check first char AFTER quoted string, if it is another
				-- quoted string without separator, then append it
				-- this is the way to "escape" the quote char in a quote. example:
				--   value1,"blub""blip""boing",value3  will result in blub"blip"boing  for the middle
			until (c ~= '"')
			table.insert(res,txt)
			assert(c == sep or c == "")
			pos = pos + 1
		else	
			-- no quotes used, just look for the first separator
			local startp,endp = string.find(line,sep,pos)
			if (startp) then 
				table.insert(res,string.sub(line,pos,startp-1))
				pos = endp + 1
			else
				-- no separator found -> use rest of string and terminate
				table.insert(res,string.sub(line,pos))
				break
			end 
		end
	end
	return res
end